# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
import requests

from models import Weather


class OpenWeatherImporter(object):

    def read_process_data(self):

        """
        This function get data from openweathermap.org and save them to db
        """
        url = "http://api.openweathermap.org/data/2.5/weather?q=London,uk&APPID=26dfc81a2513bde0aef86ed9a0f66a4e"
        response = requests.get(url)
        data = response.text
        parsed_data = json.loads(data)

        Weather.objects.create(
            latitude=parsed_data["coord"]["lat"],
            longitude=parsed_data["coord"]["lon"],
            main=parsed_data["weather"][0]["main"],
            description=parsed_data["weather"][0]["description"],
            temp=parsed_data["main"]["temp"],
            temp_min=parsed_data["main"]["temp_min"],
            temp_max=parsed_data["main"]["temp_max"],
            pressure=parsed_data["main"]["pressure"],
            humidity=parsed_data["main"]["humidity"],
            wind_speed=parsed_data["wind"]["speed"],
            wind_deg=parsed_data["wind"]["deg"],
            country=parsed_data["sys"]["country"],
            sunrise=parsed_data["sys"]["sunrise"],
            sunset=parsed_data["sys"]["sunset"],
            city=parsed_data["name"],
        )
