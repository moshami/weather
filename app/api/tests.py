# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework.test import APIClient, APITestCase
from rest_framework import status

from views import WeatherSerializer
from models import Weather


class TestAPI(APITestCase):

    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create(username='lauren')
        self.client.force_authenticate(user=self.user)
        self.weather_1 = Weather.objects.create(
            latitude='121212',
            longitude='2342342',
            main='rain',
            description='light rain',
            temp='278',
            temp_min='2344',
            temp_max='45656',
            pressure='234234',
            humidity='55',
            wind_speed='4.1',
            wind_deg='45',
            country='UK',
            sunrise='1552026624',
            sunset='552067583',
            city='London',
        )
        self.weather_2 = Weather.objects.create(
            latitude='125412',
            longitude='2343442',
            main='rain',
            description='light snow',
            temp='278',
            temp_min='2344',
            temp_max='45656',
            pressure='234234',
            humidity='55',
            wind_speed='4.1',
            wind_deg='45',
            country='UK',
            sunrise='1552026624',
            sunset='552067583',
            city='London',
        )

    def test_model_found(self):
        self.assertEqual(self.weather_1.__str__(), u'{} Weather at {}'.format(
            self.weather_1.city, self.weather_1.created))

    def test_model_can_create_a_weather(self):
        count = Weather.objects.count()
        self.assertEqual(count, 2)

    def test_api_can_get_weather(self):
        response = self.client.get(
            reverse('weather_details',
            kwargs={'object_id': self.weather_1.id}), format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_api_can_not_update_weather(self):

        change_weather = {'main': 'snow'}
        response = self.client.put(
            reverse('weather_details', kwargs={'object_id': self.weather_2.id}),
            change_weather, format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_api_can_not_delete_weather(self):

        response = self.client.delete(
            reverse('weather_details', kwargs={'object_id': self.weather_2.id}),
            format='json',
            follow=True)

        self.assertEquals(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_can_get_a_weather_list(self):
        response = self.client.get(reverse('weather_list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_weather_list_serializer(self):
        response = self.client.get(reverse('weather_list'))
        weather_listing = Weather.objects.all()
        serialized = WeatherSerializer(weather_listing, many=True)
        self.assertEqual(response.data, serialized.data)
