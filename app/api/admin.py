# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.contrib import admin

from rangefilter.filter import DateRangeFilter

from models import Weather


class WeatherAdmin(admin.ModelAdmin):

    model = Weather
    list_display = (
        'city',
        'created',
    )
    search_fields = [
        'id',
        'city',
        'country',
        'created',
    ]
    list_filter = (
        ('created', DateRangeFilter),
    )
    readonly_fields = (
        'created',
    )
    list_per_page = 50
    ordering = ['id']

    fieldsets = (
        (_('GeInfo'), {
            'fields': (
                'city',
                'country',
                'latitude',
                'longitude',
            )
        }),
        (_('Weather'), {
            'fields': (
                'main',
                'description',
                'temp',
                'temp_min',
                'temp_max',
                'pressure',
                'humidity',
                'wind_speed',
                'wind_deg',
                'sunrise',
                'sunset',
            )
        }),
        (_('Created'), {
            'fields': (
                'created',
            )
        }),
    )


admin.site.register(Weather, WeatherAdmin)
