# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework import serializers
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from models import Weather


class WeatherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Weather
        fields = '__all__'


class WeatherList(APIView):

    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    def get(self, request):

        weather_listing = Weather.objects.filter()

        serializer = WeatherSerializer(weather_listing, many=True)

        return Response(serializer.data)


class WeatherDetails(APIView):

    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    def get(self, request, object_id):
        consumer = Weather.objects.get(pk=object_id)
        serializer = WeatherSerializer(consumer)

        return Response(serializer.data)
