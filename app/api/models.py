# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _


class Weather(models.Model):
    main = models.CharField(
        verbose_name=_('Climate'),
        max_length=255,
        blank=True,
        null=True,
    )
    latitude = models.FloatField(
        verbose_name=_('Latitude'),
        blank=True,
        null=True,
    )
    longitude = models.FloatField(
        verbose_name=_('Longitude'),
        blank=True,
        null=True,
    )
    description = models.CharField(
        verbose_name=_('Description'),
        max_length=255,
        blank=True,
        null=True,
    )
    temp = models.FloatField(
        verbose_name=_('Temperature'),
        blank=True,
        null=True,
    )
    temp_min = models.FloatField(
        verbose_name=_('Temperature Min'),
        blank=True,
        null=True,
    )
    temp_max = models.FloatField(
        verbose_name=_('Temperature Max'),
        blank=True,
        null=True,
    )
    pressure = models.IntegerField(
        verbose_name=_('Pressure'),
        blank=True,
        null=True,
    )
    humidity = models.IntegerField(
        verbose_name=_('Humidity'),
        blank=True,
        null=True,
    )
    wind_speed = models.FloatField(
        verbose_name=_('Wind Speed'),
        blank=True,
        null=True,
    )
    wind_deg = models.IntegerField(
        verbose_name=_('Wind Deg'),
        blank=True,
        null=True,
    )
    country = models.CharField(
        verbose_name=_('Country'),
        max_length=255,
    )
    city = models.CharField(
        verbose_name=_('City'),
        max_length=255,
    )
    sunrise = models.IntegerField(
        verbose_name=_('Sunrise'),
        blank=True,
        null=True,
    )
    sunset = models.IntegerField(
        verbose_name=_('Sunset'),
        blank=True,
        null=True,
    )
    created = models.DateTimeField(
        verbose_name=_('Creation date/time'),
        editable=False,
        auto_now_add=True,
        help_text=_('Object creation date and time')
    )

    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return "{} Weather at {}".format(self.city, self.created)
