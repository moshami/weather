from django.conf.urls import url

from api.views import WeatherList, WeatherDetails

urlpatterns = [
    url(r'^weather/(?P<object_id>[0-9]+)$', WeatherDetails.as_view(), name='weather_details'),
    url(r'^weather-listing/', WeatherList.as_view(), name='weather_listing'),
]
