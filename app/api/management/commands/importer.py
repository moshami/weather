from django.core.management.base import BaseCommand

from api.utils import OpenWeatherImporter


class Command(BaseCommand):
    help = 'import data'

    def handle(self, *args, **options):
        self.stdout.write(self.style.NOTICE('Start reading and processing weather data'))
        processor = OpenWeatherImporter()
        processor.read_process_data()
        self.stdout.write(self.style.SUCCESS(
            'Reading and processing weather data has been successful'))

