SPARK Solution Report
====


## Data Modeling:


#### There is a model `WEATHER`.


** `weather object`, fields are latitude,longitude,main,description,temp,temp_min,temp_max,
pressure,humidity,wind_speed,wind_deg,country,sunrise,sunset, created

## Importer:


**  there is a management command to read, process and store the data from `openweathermap.org`

*** `read_process_data()` , It uses `requests` to download data and store them in db using `djano ORM`


## Views:


#### 'WeatherSerializer'

* It is a `ModelSerializer`
* It gets all the Weather data and serialize them

#### 'WeatherList'

* It is a `APIView`
* It gets all Weather data/serialize
* It creates response 

#### 'WeatherDetail'

* It is a `APIView`
* It gets a specific Weather data/serialize using `object_id`
* It creates response 


## Tests:

#### `TestAPI`

* It covers models and views.

## Admin:

* It covers weather objects.

## FrontEnd:

* `JQuery` has been used to communicate with the REST API in the backend and display the stored weather data.
* `AJAX` has been defined to get the data and insert therm into table in home page


## How to set it up:

* download the source code into you dev folder
* create a virtual env for it
* install the requirements `pip install -r requirements.txt`
* `cd app` and run migrations `python manage.py migrate`
* populate the db by running a script `python manage.py importer`
* run your project `python manage.py runserver`
* run your project `python manage.py runserver`
* go to your browser `http://localhost:8000/`
* `signup` or `login`
* in order to test weather_list endpoint, go to your browser `http://localhost:8000/api/weather-listing/`
* in order to test weather_detail endpoint, go to your 
    browser `http://localhost:8000/api/weather/1` , You could change`[id]` on it
* in order to run tests `python manage.py test`
 
* PLease Not, I have used vagrant to develop this project but I have added docker config as well, However is hasn't been tested properly

```shell

# For Vagrant
vagrant up
vagrant ssh
# Inside the box
workon weather
./manage.py runserver

# For Docker
docker-compose up
# You can run `manage.py` commands using the `./manapy` wrapper 
```

## How to poll the OpenWeatherMap API on an hourly basis to get the current weather data for London, and save to db  

* There is a `crontab` in `weather/import_weather_data_schedule_task` file, which needs to be added 
to celery server to download and store the data every hour to db

## How to test the api endpoint using postman

* run your server locally
* go to postman
* go to Authorization
* login in 
* add `http://localhost:8000/api/weather-listing/` using GET method 
