FROM alpine

RUN apk add --update py-pip python postgresql postgresql-dev zlib-dev libjpeg-turbo-dev gcc python-dev musl-dev make \
 && pip install --upgrade pip
RUN if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python /usr/bin/python; fi

WORKDIR /srv/weather
ADD requirements.txt /weather/
RUN pip3 install -r requirements.txt